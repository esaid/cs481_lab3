import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

//Lab 3 - Chips, buttons and card, EC-Dialog boxes or languages
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid-19 Symptom Checker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: UserInfo(),
    );
  }
}

class  UserInfo extends StatefulWidget {
  UserInfo({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _UserInfoState();
}
class _UserInfoState extends State<UserInfo> {
  static String gender = ''; //card with chips inside
  static int age = 1;//dropdown
  static bool overweight, diabetes, respDisease, heartDisease;//filter chips
  static List choiceGender = ['Male', 'Female'];
  int value;//for gender selection
  @override

  Widget build(BuildContext context) {
    //implement bottom navigation
    return Scaffold(
      backgroundColor: new Color(0xffe0f5ff),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            showAboutDialog(
                context: context,
                applicationIcon:
                Icon(Icons.ac_unit, size: 60, color: new Color(0xffb2e8ff)),
                applicationName: 'Covid-19 Symptom Checker',
                applicationVersion: '1.0.0',
                applicationLegalese: 'Developed by Wolf, Wizard, Zsuzsanna');
          },
        ),
        title: Text('Covid-19 Symptom Checker'),
        actions: <Widget>[],
      ),
      body: Container(
        child: Column(
          children: [
          //Gender
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text('Gender', style: TextStyle(fontWeight: FontWeight.bold))],),
          Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
          width: 300,
          height: 100,
          child: Card(
            shape: ContinuousRectangleBorder(),
            color: new Color(0xffe0f5ff),
            elevation: 20,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List<Widget>.generate(2, (int index) {
              return FilterChip(
                label: Text(choiceGender.elementAt(index)),
                backgroundColor: Colors.blue[200], 
                selectedColor: Colors.grey[600],
                selected: value == index,
                onSelected: (bool selected) {
                  setState(() {value = selected ? index: null;
                  if(selected){gender = 'Male';}
                  else if(!selected){gender = 'Female';}
                  else{gender = '';}
                  print(choiceGender.elementAt(index) + ' has been selected.');
                  });
                },
                );}
                ).toList(),
                ),
            ),
          ),]//inner children
      ),

//Age dropdown
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text('Age', style: TextStyle(fontWeight: FontWeight.bold))],),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
          width: 300,
          height: 100,
          child: Card(
            shape: ContinuousRectangleBorder(),
            color: new Color(0xffe0f5ff),
            elevation: 20,
            child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [DropdownButton<int>(
            value: age,
            icon: Icon(Icons.arrow_downward, color: Colors.blueAccent),
            iconSize: 24,
            elevation: 20,
            style: TextStyle(
            color: Colors.blue
            ),
            underline: Container(
            height: 2,
            color: Colors.blueAccent,
            ),
            onChanged: (int newValue) {
            setState(() {
              age = newValue;
              print('Age: ' + age.toString());
            });
    },
            items: <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                        40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 
                        70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100]
            .map<DropdownMenuItem<int>>((int value) {
              return DropdownMenuItem<int>(
                value: value,
                child: Text(value.toString()),
        );
        }).toList(),
  ),]
  ),
        
          ),
          ), 
        ],//inner children
      ),



          //Co-morbidities
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text('Co-morbidities', style: TextStyle(fontWeight: FontWeight.bold))],),
          Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 300,
            height: 150,
            child: ListView(
              children: [
                Card(
                  shape: ContinuousRectangleBorder(),
                  color: new Color(0xffe0f5ff),
                  elevation: 20,
                  child: Wrap(
                    children: <Widget>[
                      Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        CoMordbidWidget(name: 'Overweight'),
                        CoMordbidWidget(name: 'Diabetes'),
                          ],
                      ),
                      Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                       CoMordbidWidget(name: 'Resp. disease'),
                       CoMordbidWidget(name: 'Heart disease'),
                      ],
                      ),
                    ],
                  ),
                ),],
            ),
          ),
          ],
      ),

          //Go to next page
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [RaisedButton(
              child: Text('Enter Symptoms'),
              color: Colors.blue[200],
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SymptomScreen()));
              },
            ),]
          ),
        ]),
      ),
    );
  }
}
class CoMordbidWidget extends StatefulWidget {
  final String name;
  CoMordbidWidget({Key key, this.name}) : super(key: key);
  @override
  _CoMordbidWidgetState createState() => _CoMordbidWidgetState();
}
class _CoMordbidWidgetState extends State<CoMordbidWidget> {
  bool _isSelect = false;
  @override
  Widget build(BuildContext context) {
    return FilterChip(
      label: Text(widget.name),
      backgroundColor: Colors.blue[200], 
      selectedColor: Colors.grey[600],
      selected: _isSelect,
      onSelected: (isSelected) {
        setState(() {
          _isSelect = isSelected;
          if(widget.name == 'Overweight'){_UserInfoState.overweight = _isSelect;}
          if(widget.name == 'Diabetes'){_UserInfoState.diabetes = _isSelect;}
          if(widget.name == 'Resp. disease'){_UserInfoState.respDisease = _isSelect;}
          if(widget.name == 'Heart disease'){_UserInfoState.heartDisease = _isSelect;}
           print(
             //prints current selections
             'Overweight: '     + _UserInfoState.overweight.toString() + ' | '
             + ' Diabetes: '      + _UserInfoState.diabetes.toString() + ' | '
             + ' Resp. disease: ' +_UserInfoState.respDisease.toString() + ' | '
             + ' Heart disease: ' +_UserInfoState.heartDisease.toString());
        });
      },
    );
  }
}



//Wolf
class SymptomScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {},
        ),
        title: Text('Symptoms'),
        actions: <Widget>[],
      ),
      body: Center(
          child: RaisedButton(
            child: Text('Check Results'),
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Results()));
            },
          )),
    );
  }
}

//Zsuzsanna
class Results extends StatelessWidget {
  var _symptomCounter = 8;
  var _symptomTotal = 1;
  var _CoMordbidWidgetitiesTotal = 0;
  var _age = 79;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffb2e8ff),
      appBar: AppBar(
        backgroundColor: new Color(0xffb2e8ff),
        centerTitle: true,
        title: Text(
          'Results',
          style: TextStyle(color: Colors.red),
        ),
        leading: IconButton(
          icon: Icon(Icons.repeat),
          onPressed: () {
            showAlertDialog(context);
          },
        ),
        // title: Text('Results'),
        //actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: 20,
                ),
                // child: Text('Results', style: TextStyle(fontSize: 30,color: Colors.deepOrange, ),),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                    color: new Color(0xffe0f5ff),
                    child: tableToUse(_symptomCounter)),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                  color: new Color(0xffe0f5ff),
                  child: cardToUse(_symptomTotal, _CoMordbidWidgetitiesTotal),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                  color: new Color(0xffe0f5ff),
                  child: ageCardToUse(_age),
                ),
              ),
              RaisedButton.icon(
                color: new Color(0xffe0f5ff),
                icon: Icon(Icons.repeat),
                hoverColor: Colors.deepOrange,
                label: Text(
                  'Retry',
                  style: TextStyle(fontSize: 20),
                ),
                onPressed: () {
                      showAlertDialog(context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> showAlertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            //idea and some code from StackOverflow
            content: new Container(
              width: 260.0,
              height: 275.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
              ),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  // dialog top
                  new Expanded(
                    child: new Row(
                      children: <Widget>[
                        Center(
                          child: new Container(
                            // padding: new EdgeInsets.all(10.0),
                            decoration: new BoxDecoration(
                              color: Colors.white,
                            ),
                            child: Text(
                              'You Are About to Erase All Input!',
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 18.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // dialog centre
                  new Expanded(
                    child: new Container(
                        child: new Text('Are you sure you want to reset?',
                          style: TextStyle(fontSize: 20),

                        )),
                    flex: 1,
                  ),

                  // dialog bottom
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                        color: const Color(0xffe0f5ff),
                      ),
                      child: FlatButton(
                        child: Text('YES, RESET', style: TextStyle(color: Colors
                            .black, fontSize: 18),),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UserInfo()));
                        },
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 10,
                  ),
                  // dialog bottom
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                        color: const Color(0xffe0f5ff),
                      ),
                      child: FlatButton(
                        child: Text('NO, BACK TO RESULTS', style: TextStyle(color: Colors
                            .black, fontSize: 18),),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),

                ],
              ),
            ),
          );
        });
  }

  Widget ageCardToUse(int age) {
    if (age < 60)
      return SafeAge();
    else
      return DangerAge();
  }

  Widget cardToUse(int symp, int comorbs) {
    if (symp == 1 && comorbs == 0)
      return YellowCardTwo();
    else if (symp == 0 && comorbs >= 1)
      return YellowCardOne();
    else if (symp >= 1 && comorbs >= 1)
      return RedCardOne();
    else if (symp >= 2 && comorbs == 0)
      return RedCardTwo();
    else
      return GreenCard();
  }

  Widget tableToUse(int count) {
    if (count <= 4)
      return DataTableUser4();
    else if (count <= 8)
      return DataTableUser8();
    else
      return DataTableUser12();
  }
}

class SafeAge extends StatelessWidget {
  SafeAge({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.green[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.cake,
              size: 40,
            ),
            title: Text(
              'Safe Age',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'Your age is in the population that is considered to be safer.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class DangerAge extends StatelessWidget {
  DangerAge({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.add_alert,
              size: 40,
            ),
            title: Text(
              'Cautious Age',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'Your age is in the section of the population that needs to practice safer protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class RedCardOne extends StatelessWidget {
  RedCardOne({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.bug_report,
              size: 40,
            ),
            title: Text(
              'Seek Medical Advice',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing at least one symptom and have at least one co-morbidity. Seek medical advice as soon as possible.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class RedCardTwo extends StatelessWidget {
  RedCardTwo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.bug_report,
              size: 40,
            ),
            title: Text(
              'Seek Medical Advice',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing symptoms and have at least one co-morbidity. Seek medical advice as soon as possible.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class GreenCard extends StatelessWidget {
  GreenCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.green[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.check,
              size: 40,
            ),
            title: Text(
              'No Signs of Infection',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing no symptoms. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class YellowCardOne extends StatelessWidget {
  YellowCardOne({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.yellow[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.help_outline,
              size: 40,
            ),
            title: Text(
              'Caution Advised',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing no symptoms, but have at least one co-mobidity. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class YellowCardTwo extends StatelessWidget {
  YellowCardTwo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.yellow[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.help_outline,
              size: 40,
            ),
            title: Text(
              'Caution Advised',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing a symptom. It is not possible to determine an infection. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class DataTableUser12 extends StatelessWidget {
  DataTableUser12({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columnSpacing: 15,
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Input',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            ' ',
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Gender',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('female', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Age',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('43', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Co-morbidities',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('weight')),
            DataCell(Text('diabetes')),
            DataCell(Text('resp. disease')),
            DataCell(Text('heart disease')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Symptoms',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
      ],
    );
  }
}

class DataTableUser8 extends StatelessWidget {
  DataTableUser8({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columnSpacing: 15,
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Input',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            ' ',
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Gender',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('female', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Age',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('43', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Co-morbidities',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('weight')),
            DataCell(Text('diabetes')),
            DataCell(Text('resp. disease')),
            DataCell(Text('heart disease')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Symptoms',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
      ],
    );
  }
}

class DataTableUser4 extends StatelessWidget {
  DataTableUser4({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columnSpacing: 15,
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Input',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            ' ',
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Gender',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('female', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Age',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('43', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Co-morbidities',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('weight')),
            DataCell(Text('diabetes')),
            DataCell(Text('resp. disease')),
            DataCell(Text('heart disease')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Symptoms',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
      ],
    );
  }
}
